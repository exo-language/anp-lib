#ifndef EXO_PLUGIN_API_DETAIL_H
#define EXO_PLUGIN_API_DETAIL_H

#include <cstdint>

namespace exo::anp::detail {
    extern "C" uint8_t* allocate(std::size_t const size);
    extern "C" void deallocate(uint8_t const* const pointer);

    extern "C" uint8_t* visit(
        char const* const category_identifier,
        char const* const ast,
        uint8_t const* const args);
}

#endif
