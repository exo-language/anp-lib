#ifndef EXO_ANP_API_EITHER_HPP
#define EXO_ANP_API_EITHER_HPP

#include <cassert>
#include <type_traits>

namespace exo {
    template <typename L, typename R>
    struct Either {
        Either(Either<L, R> const& that):
            _is_left { that._is_left }
        {
            if (_is_left) _left_right._left = that._left_right._left;
            else _left_right._right = that._left_right._right;
        }

        Either(Either<L, R>&& that):
            _is_left { that._is_left }
        {
            if (_is_left) _left_right._left = std::move(that._left_right._left);
            else _left_right._right = std::move(that._left_right._right);
        }

        template <typename A>
        Either(A const& left_right) requires(std::is_same_v<R, A> && !std::is_same_v<L, A>):
            _is_left { false }
        {
            _left_right._right = left_right;
        }

        template <typename A>
        Either(A const& left_right) requires(std::is_same_v<L, A> && !std::is_same_v<R, A>):
            _is_left { true }
        {
            _left_right._left = left_right;
        }

        template <typename A>
        Either(A const& left_right, bool const& is_left = false) requires(std::is_same_v<R, A> && std::is_same_v<L, A>):
            _is_left { is_left }
        {
            if (is_left) _left_right._left = left_right;
            else _left_right._right = left_right;
        }

        ~Either()
        {
            if (_is_left) _left_right._left.~L();
            else _left_right._right.~R();
        }

        L const& as_left() const {
            assert(is_left());
            return _left_right._left;
        }

        L& as_left() {
            assert(is_left());
            return _left_right._left;
        }

        R const& as_right() const {
            assert(is_right());
            return _left_right._right;
        }

        R& as_right() {
            assert(is_right());
            return _left_right._right;
        }

        template <typename A>
        operator Either<L, A>() const {
            return Either<L, A> { as_left() };
        }

        template <typename A>
        operator Either<A, R>() const {
            return Either<A, R> { as_right() };
        }

        bool is_left() const {
            return _is_left;
        }

        bool is_right() const {
            return !_is_left;
        }

    private:
        union LR {
            LR() {}
            ~LR() {}

            L _left;
            R _right;
        } _left_right;
        bool _is_left;
    };
}

#endif
