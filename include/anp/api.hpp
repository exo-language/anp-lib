#ifndef EXO_ANP_API_HPP
#define EXO_ANP_API_HPP

#include <anp/apiDetail.h>
#include <anp/anpTypes.hpp>
#include <anp/ast.hpp>
#include <anp/either.hpp>

#include <string>
#include <cstring>

#include <utility>

namespace exo::anp {
    template <typename A>
    [[maybe_unused]] static uint8_t* allocate(A const& value) {
        uint8_t* const& adress { detail::allocate(sizeof(A)) };
        return reinterpret_cast<uint8_t*>(new (adress) A(value));
    }

    [[maybe_unused]] static uint8_t* allocate_string(std::string const& string) {
        auto const& c_string { detail::allocate(string.size() + 1) };
        memcpy(c_string, string.c_str(), string.size() + 1);
        return reinterpret_cast<uint8_t*>(c_string);
    }

    template <typename Return, typename Args>
    [[maybe_unused]] static exo::Either<char const*, Return> visit(char const* const& category, char const* const& goal, Ast const& ast, Args const& args) {
        using ReturnEither = exo::Either<char const*, Return>;
        using namespace std::literals::string_literals;
        auto const& category_identifier { category + ":"s + goal };

        uint8_t* const& adress { detail::visit(category_identifier.c_str(), ast, reinterpret_cast<uint8_t const*>(&args)) };
        ReturnEither result = std::move(*reinterpret_cast<ReturnEither*>(adress));
        detail::deallocate(adress);
        return result;
    }
}

#define AST_NODE_PLUGIN(anp_type, anp_category, anp_goal) \
namespace exo::anp { \
    using SelectAst = decltype(anp_type.select_ast(std::declval<Ast>())); \
    static uint8_t* comfy_visit(SelectAst const& ast, uint8_t const* const& args); \
 \
    namespace detail { \
        static std::string const string_identifier = anp_type.identifier() + ":" anp_category ":" anp_goal; \
        extern "C" char const* const identifier = string_identifier.c_str(); \
 \
        extern "C" uint8_t* visit_node(char const* const ast, uint8_t const* const args) { \
            return comfy_visit(anp_type.select_ast(ast), args); \
        } \
    } \
} \
 \
static uint8_t* exo::anp::comfy_visit(SelectAst const& ast, uint8_t const* const& args)

#endif
