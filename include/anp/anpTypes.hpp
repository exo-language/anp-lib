#ifndef EXO_ANP_API_TYPES_HPP
#define EXO_ANP_API_TYPES_HPP

#include <anp/ast.hpp>
#include <string>

namespace exo::anp {
    struct ForwardAnpType {
        std::string identifier() const {
            return "forward_dispatch";
        }

        auto select_ast(Ast const& ast) const {
            return ast;
        }
    } const forward_anp_type;

    struct DispatchAnpType {
        std::string identifier() const {
            return "dispatch(" + std::string { _symbol } + ")";
        }

        auto select_ast(Ast const& ast) const {
            return ast.as_symbol();
        }

        std::string_view _symbol;
    };

    struct BindingDispatchAnpType {
        std::string identifier() const {
            return "binding_dispatch(" + std::string { _actor_symbol } + ")";
        }

        auto select_ast(Ast const& ast) const {
            return ast.as_binding();
        }

        std::string_view _actor_symbol;
    };

    struct ListAnpType {
        std::string identifier() const {
            return "list_dispatch";
        }

        auto select_ast(Ast const& ast) const {
            return ast.as_list();
        }
    } const list_anp_type;

    struct LiteralAnpType {
        std::string identifier() const {
            return "literal_dispatch";
        }

        auto select_ast(Ast const& ast) const {
            return ast.as_literal();
        }
    } const literal_anp_type;
}

#endif
