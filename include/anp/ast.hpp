#ifndef EXO_ANP_API_AST_HPP
#define EXO_ANP_API_AST_HPP

#include <string_view>
#include <vector>
#include <tuple>
#include <cassert>

namespace exo {
    struct Ast final {
        Ast(char const* const& raw_ast):
            _raw_ast { raw_ast }
        {}

        std::tuple<std::string_view, Ast> as_symbol() const {
            assert(is_symbol());

            char const* const& literal_start { _raw_ast };
            char const* literal_end { literal_start };
            while (*literal_end != '.') {
                assert(*literal_end != '\0');
                ++literal_end;
            }

            auto const& symbol_size { static_cast<std::string_view::size_type>(literal_end - literal_start) };
            std::string_view const symbol { literal_start, symbol_size };
            Ast const remainder { literal_end + 1 };
            return std::make_tuple(symbol, remainder);
        }

        std::string_view as_literal() const {
            assert(is_literal());

            char const* const& literal_start { _raw_ast + 1 };
            char const* literal_end { literal_start };
            while (*literal_end != '"') {
                assert(*literal_end != '\0');
                if (*literal_end != '\\') ++literal_end;
                else literal_end += 2;
            }

            auto const& symbol_size { static_cast<std::string_view::size_type>(literal_end - literal_start) };
            return std::string_view { literal_start, symbol_size };
        }

        std::vector<Ast> as_list() const {
            assert(is_list());

            std::vector<Ast> result;
            char const* current_position { _raw_ast + 1 };
            if (*current_position == ']') return result;
            else result.emplace_back(current_position);

            uint32_t is_nested { 0 };

            while (*current_position != ']' || is_nested) {
                assert(*current_position != '\0');

                if (*current_position == '"') {
                    Ast const literal_ast { current_position };
                    current_position += literal_ast.as_literal().size();
                }
                else if (*current_position == '[') ++is_nested;
                else if (*current_position == ']') --is_nested;
                else if (*current_position == ',' && !is_nested) {
                    result.emplace_back(current_position + 1);
                }

                ++current_position;
            }

            return result;
        }

        std::tuple<Ast, Ast> as_binding() const {
            assert(is_binding());

            char const* current_position { _raw_ast + 1 };
            Ast const actor { current_position };

            uint32_t is_nested { 0 };

            while (true) {
                assert(*current_position != '\0');

                if (*current_position == '"') {
                    Ast const literal_ast { current_position };
                    current_position += literal_ast.as_literal().size();
                }
                else if (*current_position == '(') ++is_nested;
                else if (*current_position == ')') --is_nested;
                else if (*current_position == '|' && !is_nested) {
                    Ast const target { current_position + 1 };
                    return std::make_tuple(actor, target);
                }

                ++current_position;
            }
        }

        bool is_symbol() const {
            return !is_literal() && !is_list() && !is_binding();
        }

        bool is_literal() const {
            return *_raw_ast == '"';
        }

        bool is_list() const {
            return *_raw_ast == '[';
        }

        bool is_binding() const {
            return *_raw_ast == '(';
        }

        operator char const*() const {
            return _raw_ast;
        }

    private:
        char const* _raw_ast;
    };
}

#endif
